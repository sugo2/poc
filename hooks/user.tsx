import { useEffect, useContext } from 'react'
import { useAuthContext } from './context';
import useSporran from './sporran'

export default function useUser() {
  // TODO: Refactor the useSporran hook, atm it's NOT reliable on Next.js > 12 || React > 17
  const { sporran, sessionObject, startSession, presentCredential } =
    useSporran()

  const { user, setUser } = useAuthContext();
  // TODO: Check if it's better to use useSWR instread of fetch inside useEffect
  useEffect(() => {
    // declare the async data fetching function
    const fetchData = async () => {
      // get the data from the api
      const data = await fetch('/api/user', {
        credentials: 'include',
      });
      // convert the data to json
      const json = await data.json();
      setUser(!!json ? json : null)
    }

    // call the function
    fetchData()
      // make sure to catch any error
      .catch(console.error);
  }, [sessionObject, user, setUser])

  async function logout() {
    const loggedOut = (await fetch('/api/logout', { credentials: 'include' }))
      .ok
    if (!loggedOut) return
    setUser(null)
  }

  async function login() {
    if (!sporran) return
    if (!sessionObject) return await startSession()

    await presentCredential()

    setTimeout(async () => {
      const result = await (
        await fetch('/api/user', { credentials: 'include' })
      ).text()
      setUser(!!result ? result : null)
    }, 500)
  }

  return {
    user,
    connected: !!user || !!sessionObject,
    login,
    logout,
  }
}
