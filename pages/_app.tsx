import { AppProps } from 'next/app'
import { AuthProvider } from '../hooks/context'

export default function App({ Component, pageProps }: AppProps) {
  return <AuthProvider>
    <Component {...pageProps} />
  </AuthProvider>
}