import { useRouter } from "next/router"
import Logo from "../../components/Logo"
import User from "../../components/User"
import useUser from "../../hooks/user"
import { useEffect } from "react"

export default function Secret() {
  const { user, connected, login, logout } = useUser()
  const router = useRouter()

  useEffect(() => {
    if (!user) router.push('/')
  }, [user, router])

  return (
    <>
      <header>
        <Logo />
        <User user={user} connected={connected} onClick={user ? logout : login} />
      </header>
      <main>
        <div>
          <h1>Top Secret Page</h1>
          <button onClick={() => router.push('/')}>BACK HOME</button>
        </div>
      </main>
    </>
  )
}
