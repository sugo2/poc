import { useRouter } from 'next/router'
import Logo from '../components/Logo'
import User from '../components/User'
import useUser from '../hooks/user'

export default function Home() {
  const { user, connected, login, logout } = useUser()
  const router = useRouter()

  async function testSecretApi() {
    const result = await fetch('/api/secret', { credentials: 'include' })
    const message = await result.text()
    alert(message)
  }

  async function testSecretPage() {
    router.push('/secret/secret')
  }

  return (
    <>
      <header>
        <Logo />
        <User
          user={user}
          connected={connected}
          onClick={user ? logout : login}
        />
      </header>
      <main>
        <div>
          <button onClick={testSecretPage}>GO TO SECRET PAGE</button>
          <button onClick={testSecretApi}>GET SECRET MESSAGE</button>
          <button onClick={logout}>CLEAR COOKIES</button>
        </div>
      </main>
    </>
  )
}
