export default function User({ user, connected, onClick }) {
  return (
    <div>
      <div >
        <span>{user || 'guest'}</span>
      </div>
      <button onClick={onClick}>
        {!connected ? 'connect' : user ? 'logout' : 'login'}
      </button>
    </div>
  )
}
